<?php


namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class BlogController extends AbstractController
{

    /**
     * @Route ("", name="index")
     */
    public function addArticle(Request $request)
    {
        $repository = new PostRepository();
        $title = $request->get('title');
        $author = $request->get('author');
        // $postDate = $request->get('postDate');
        $content = $request->get('content');
        $firstTitle = 'FlyDreams';

        if ($title && $author && $content) {
            $post = new Post($title, $author, new \DateTime(), $content);
            $repository->add($post);
        }

        $postTab = $repository->findAll();

        return $this->render('blog.html.twig', [
            'postTab' => $postTab,
            'firstTitle' => $firstTitle,
        ]);
    }


    /**
     * @Route ("/form-blog", name ="form_blog")
     */
    public function ajouterArticle(Request $request)
    {
        $post = null;
        $title = $request->get('title');
        $author = $request->get('author');
        $content = $request->get('content');
        if ($title && $author && $content) {
            $post = new Post($title, $author, new \DateTime(), $content);
            var_dump($post);
            $postRepository = new PostRepository();
            $postRepository->add($post);
            return $this->redirectToRoute('index');
        }


        return $this->render('form.html.twig', [
            'post' => $post
        ]);
    }

    /**
     * @Route ("/modifArticle/{id}", name="modif_Article")
     */
    public function modifArticle(Request $request, PostRepository $repo, int $id)
    {
        $firstTitle = 'FlyDreams';
        $oldPost = $repo->findById($id);
        $title = $request->get("title");
        $content = $request->get("content");
        $author = $request->get("author");



        if ($title   && $author && $content) {
            $repo->update($id, $content, $title, $author);
            return $this->redirectToRoute('index');
        }
        return $this->render('modifArticle.html.twig', [
            'firstTitle' => $firstTitle,
            'oldPost' => $oldPost
        ]);
    }

    // /**
    //  * @Route ("/photo", name="photo")
    //  */
    // // public function addPhoto()

    // // {
    // //     $firstTitle = 'FlyDreams';

    // //     return $this->render('photo.html.twig', [
    // //         'firstTitle' => $firstTitle
    // //     ]);
    // // }

    /**
     * @Route ("/article/{id}", name="voir_article")
     */

    public function showId(int $id)
    {

        $firstTitle = 'FlyDreams';
        $repository = new PostRepository();
        $article = $repository->findById($id);
        return $this->render('article.html.twig', [

            'article' => $article,
            'firstTitle' => $firstTitle

        ]);
    }
    /**
     * @Route ("/post/delete/{id}", name="delete_post")
     */

    public function deletePost(PostRepository $repo, int $id)
    {
        $repo->delete($id);
        return $this->redirectToRoute('index');
    }
}
