<?php

namespace App\Form;

use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class PostType extends AbstractType{

    public function buildFom(FormBuilderInterface $builder, array $option){

        $builder->add('title')
                ->add('author')
                // ->add('postDate',DateTimeType::class)
                ->add('content',TextareaType::class);
    }

    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver){

        $resolver->setDefaults([
            'data_class'=> Post::class
        ]);
    }
}