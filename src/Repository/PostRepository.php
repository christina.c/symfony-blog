<?php

namespace App\Repository;

use App\Entity\Post;

class PostRepository
{
    private $pdo;

    public function __construct() {

        $this->pdo = new \PDO(
            'mysql:host='.$_ENV['DATABASE_HOST'] .';dbname=' . $_ENV['DATABASE_NAME'],
            $_ENV['DATABASE_USERNAME'],
            $_ENV ['DATABASE_PASSWORD']

        );
    }
    public function findAll(): array
    {
        $query = $this->pdo->prepare('SELECT * FROM Post');
    
        $query->execute();

        $results = $query->fetchAll();
        $list = [];
   
        foreach ($results as $line) {
  
            $post = $this->sqlToPost($line);

            $list[] = $post;
        }
   
        return $list;
    }

    public function add(Post $post): void {
 
        $query = $this->pdo->prepare('INSERT INTO Post (title, author, postDate, content) VALUES (:title,:author,:postDate,:content)');
  
        $query->bindValue('title', $post->getTitle(), \PDO::PARAM_STR);
        $query->bindValue('author', $post->getAuthor(), \PDO::PARAM_STR);
        $query->bindValue('postDate', $post->getPostDate()->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
        $query->bindValue('content', $post->getContent(), \PDO::PARAM_STR);
       
        $query->execute();

        $post->setId(intval($this->pdo->lastInsertId()));
    }


    public function findById(int $id): ?Post {

        $query = $this->pdo->prepare('SELECT * FROM Post WHERE id=:id');

        $query->bindValue(':id', $id, \PDO::PARAM_INT);

        $query->execute();
        $line = $query->fetch();
        if($line) {
            return $this->sqlToPost($line);
        }
    
        return null;

    }

    private function sqlToPost(array $line):Post {
        $resultDate = \DateTime::createFromFormat('Y-m-d H:i:s', $line['postDate']);
        return new Post($line['title'], $line['author'], $resultDate, $line['content'], $line['id']);
    }

    public function delete(int $id){
        
        $delete = $this->pdo->prepare('DELETE FROM Post WHERE id=:id');
        $delete -> bindValue(':id', $id, \PDO::PARAM_INT);
        $delete->execute();
    }
    
    public function update(int $id,string $content, string $title, string $author){

        $update = $this->pdo->prepare('UPDATE Post SET title=:title, author=:author, content=:content WHERE id=:id');
        $update -> bindValue(':title', $title, \PDO::PARAM_STR);
        $update -> bindValue(':author', $author, \PDO::PARAM_STR);
        $update -> bindValue(':content', $content, \PDO::PARAM_STR);
        $update -> bindValue(':id', $id, \PDO::PARAM_INT);
        $update -> execute();
    }
}
