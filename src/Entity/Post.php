<?php

namespace App\Entity;

use DateTime;

class Post
{

    private $id;
    private $title;
    private $author;
    private $postDate;
    private $content;

    public function __construct(string $title, string $author, \DateTime $postDate, string $content, int $id = null)
    {
        $this->id = $id;
        $this->title = $title;
        $this->author = $author;
        $this->postDate = $postDate;
        $this->content = $content;
    }
    public function getTitle(): string
    {
        return $this->title;
    }

        public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getAuthor(): string
    {
        return $this->author;
    }

        public function setAuthor(int $author): void
    {
        $this->author = $author;
    }

    public function getPostDate(): DateTime
    {
        return $this->postDate;
    }

       public function setPostDate(DateTime $postDate): void
    {
        $this->postDate = $postDate;
    }


    public function getContent(): string
    {
        return $this->content;
    }

       public function setContent(string $content): void
    {
        $this->content = $content;
    }


    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }
}
