DROP DATABASE IF EXISTS blog;

CREATE DATABASE blog;

USE blog;

CREATE TABLE Post(

    id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    title VARCHAR(15),
    author VARCHAR(15),
    postDate DATETIME,
    content VARCHAR(500)
);
INSERT INTO Post (title, author,postDate,content) VALUES ('Egypte ','Chris Chabert','2019-09-30','Ce pays qui regorge de trésor et d histoire vous émerveillera');

INSERT INTO Post (title, author,postDate,content) VALUES ('New York','Chris Chabert','2019-09-29 17h00','Cette ville vous apportera tout ce dont vous avez besoin si vous aimez faire la fête');

