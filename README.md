# BLOG

## Introduction

Mon projet consistait à créer un blog avec le framework symfony, la base de donnée MariaDB avec PDO, en utilisant des requêtes SQL et le langage PHP. J'avais des contraintes telle qu'une classe déja donnée. Le blog devait permettre d'afficher des articles, en ajouter, les  supprimer ou les modifier et voir un article spécifique. Les compétences visées étaient :

- Réaliser une interface utilisateur web statique et adaptable,
- Créer une base de données,
- Développer les composants d'accès aux données,
- Développer la partie back-end d'une application web ou web mobile



## User stories

- Comme un utilisateur qui aime voyager, je  souhaite savoir quel endroit visiter pour organiser au mieux mon voyage.

- Comme un lecteur je souhaite m'informer sur les pays pour avoir une culture générale.

- Comme une personne qui ne peut pas voyager je souhaite voyager à travers les posts de ce blog.



## Maquette

## Mobile



![portable1](./maquette/portable1.png)



![portable2](./maquette/portable2.png)



![portable3](./maquette/portable3.png)



![portable4](./maquette/portable4.jpg)



## Ordinateur 

![ordinateur1](./maquette/ordinateur1.jpg)

![ordinateur2](./maquette/ordinateur2.jpg)

![ordinateur3](./maquette/ordinateur3.png)



![ordinateur4](./maquette/ordinateur4.jpg)



## UML

#### Use Cases

![uml-use-case-blog](./maquette/uml-use-case-blog.png)



#### Class

![diagramme-class-blog](./maquette/diagramme-class-blog.png)



## Code

### Controler
    /**
     * @Route ("", name="index")
     */
    public function index(Request $request)
    {
        $repository = new PostRepository();
        $title = $request->get('title');
        $author = $request->get('author');
        $content = $request->get('content');
        $firstTitle = 'FlyDreams';
    
        if($title && $author && $content) {
            $post = new Post($title, $author, new \DateTime(), $content);
            $repository->add($post);
        }
    
        $postTab = $repository->findAll();
    
        return $this->render('blog.html.twig', [
            'postTab' => $postTab,
            'firstTitle' => $firstTitle,
        ]);
    }

### Twig

        <div class="container-fluid">
        <h1 class="m-0 p-0 row col-12 col-md-12 col-sm-12 mb-5 justify-content-center ">{{firstTitle}}</h1>
    
        <div class="row col-12 justify-content-end">
    
            <a href="{{path('form_blog')}}" class="btn btn-warning btn-lg " tabindex="-1" role="button"
                aria-disabled="true">Ajouter
                article</a>
        </div>
    
        <div class='row justify-content-center p-0 m-0'>
            {% for  item in postTab %}
            <div class='col-8'>
    
                <div class="card text-center country">
    
                    <div class="card-header text-center">
                        {{item.title}}
                    </div>
    
                    <div class="card-body country">
    
                        <p class="card-text">{{item.content}}</p>
                        <p class="card-title">Author: {{item.author}}, Date:{{item.postDate|date('Y-m-d')}} </p>
                        <a href="{{path('photo')}}" class="btn btn-warning btn-lg" tabindex="-1" role="button"
                            aria-disabled="true">Voir
                            Photo</a>
                        <a href="{{path('modif_Article', {id:item.id, title:item.title, content:item.content, author:item.author})}}"
                            class="btn btn-warning btn-lg" tabindex="-1" role="button"
                            aria-disabled="true">Modified/Delete
                            Article</a>
                    </div>
    
                </div>
            </div>{% endfor %}
    
        </div>



- Dans ce code vous pouvez aperçevoir une fonction liée à un twig, il ne peut avoir qu'une fonction par twig. Cette fonction nous permet d'appeler les informations qui sont dans la base de données, puis le twig va nous permettre de les afficher, j'ai ajouteé des cards Bootstrap. 